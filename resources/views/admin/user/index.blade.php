<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<head>
	<title>User - Toko Buku</title>
</head>
<body>

<div class="container mt-3 mb-3">

	<a class="btn btn-danger" href="/admin">
		Kembali ke Admin
	</a>

	&nbsp;&nbsp;&nbsp;

	<a class="btn btn-success" href="/admin/user/create_user">
		Add User
	</a>

	<br><br>

	<h1>Halaman List User</h1>

	<table class="table thead-dark mt-3" border="2">
		<thead>
			<th>Username</th>
			<th>Email</th>
			<th>Aksi</th>
		</thead>
		@foreach($user as $usr)
		<tr>
			<tbody>
			<td>{{$usr->name}}</td>
			<td>{{$usr->email}}</td>
			<td>
				<a href="/admin/user/delete/{{$usr->id}}">Delete</a>
				&nbsp;
				<a href="/admin/user/edit/{{$usr->id}}">Edit</a>
			</td>
			</tbody>
		</tr>
		@endforeach
	</table>

</div>

</body>
</html>